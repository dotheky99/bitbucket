﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laptop.Models;
namespace Laptop.Controllers
{
    public class _clientCustomerController : Controller
    {
        laptopDataContext db = new laptopDataContext();
        // GET: _clientCustomer
        public ActionResult AccountDetail()
        {
            int key = @Convert.ToInt32(Session["ID_cus"].ToString());
            var tb = (from c in db.Customers
                      where c.ID == key
                      select c).Take(1);
            return View(tb);
        }
        public ActionResult AccountEdit()
        {
            int key = @Convert.ToInt32(Session["ID_cus"].ToString());
            var tb = from c in db.Customers
                      where c.ID == key
                      select c;
            return View(tb);
        }
        [HttpPost]
        public ActionResult AccountEdit(Customer tk)
        {
            int key = @Convert.ToInt32(Session["ID_cus"].ToString());
            string phone = Convert.ToString(Request["phone"]);
            string gender = Request["gender"];
            string name = Request["name"];
            string add = Request["address"];
            tk = db.Customers.Where(m => m.ID == key).SingleOrDefault();
            if(tk!=null)
            { 
                tk.Name = name;
                tk.Gender = gender;
                tk.Address = add;
                tk.Phone_Number = phone;
                tk.Email = Session["email"].ToString();
                tk.Status = "Active";
                tk.updated_at = DateTime.Now;
                UpdateModel(tk);
                db.SubmitChanges();
            }
            return RedirectToAction("notification", "_clientCustomer");
        }
        public ActionResult notification()
        {
            var tb = from c in db.Customers
                     where c.ID == Convert.ToInt32(Session["ID_cus"])
                     select c;
            return View(tb);
        }
        [HttpPost]
        public ActionResult DoiMK(Customer tk)
        {

            int key = @Convert.ToInt32(Session["ID_cus"].ToString());
            var tb = from c in db.Customers
                     where c.ID == Convert.ToInt32(Session["ID_cus"])
                     select c;
            Session["doimk"] = null;
            string mkcu = Request["passcu"];
            string mkmoi1 = Request["passmoi1"];
            string mkmoi2 = Request["passmoi2"];
            tk = db.Customers.Where(m => m.ID == key).SingleOrDefault();
            if (tk != null)
            {
                if (tk.Password == mkcu)
                {
                    Session["doimk"] = tk;
                }
                if (tk.Password == mkcu && mkmoi1 == mkmoi2)
                {
                    Session["doimk"] = tk;
                    tk.Password = mkmoi1;
                    tk.updated_at = DateTime.Now;
                    UpdateModel(tk);
                    db.SubmitChanges();
                }
            }
            else
            {                
                return View(tb);
            }
                
            return View(tb);
        }
    }
}